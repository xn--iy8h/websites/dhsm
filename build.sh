#

export PATH=bin:$PATH
#DEB=https://github.com/jgm/pandoc/releases/download/2.10.1/pandoc-2.10.1-1-amd64.deb
#dpkg -i $DEB
#ar p $DEB data.tar.gz | tar xvz --strip-components 2 -C $DEST
TGZ=pandoc-2.10.1-linux-amd64.tar.gz
url=https://github.com/jgm/pandoc/releases/download/2.10.1/pandoc-2.10.1-linux-amd64.tar.gz
curl -L -o $TGZ $url

xxd $TGZ | head
tar tf $TGZ | head

DEST=.
tar xvzf $TGZ --strip-components 1 -C $DEST
ls -l
pandoc -t html -o index.html index.md

# docker run --rm --volume "`pwd`:/data" --user `id -u`:`id -g` pandoc/core index.md -t html -o index.html
